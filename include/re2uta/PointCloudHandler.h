/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef POINTCLOUDHANDLER_H_
#define POINTCLOUDHANDLER_H_

#include <re2uta/DataHandler.h>
#include <re2uta_inetComms/ReqPointCloudData.h>

#include <sensor_msgs/LaserScan.h>
#include <tf/transform_listener.h>
#include <laser_geometry/laser_geometry.h>

#include <pcl/common/common_headers.h>

#include <tf/transform_listener.h>

namespace re2uta
{

typedef ::pcl::PointXYZ              PointType;
typedef ::pcl::PointCloud<PointType> PointCloud;

static const int NUM_DIGITS_TO_KEEP = 4;
static const int BASE = 127;

class PointCloudHandler : public DataHandler
{

public:
    PointCloudHandler( ros::NodeHandle node, ReducedMsgController *rmc );
    ~PointCloudHandler();

    void unpack( unsigned char* data );

private:

    ros::Subscriber m_dataSub;
    ros::Subscriber m_uiSub;
    ros::Publisher  m_uiPub;
    ros::Publisher  m_uiPrevPub;

    bool m_collectPoints;
    PointCloud m_points;
    sensor_msgs::PointCloud2 m_prevPointsMsg;
    laser_geometry::LaserProjection m_laserProjector;
    tf::TransformListener m_tfListener;

    float m_rotationSpeed;

    ros::ServiceClient client_;
    tf::TransformListener tf_listener;

    float m_xMin;
    float m_xMax;
    float m_yMin;
    float m_yMax;
    float m_zMin;
    float m_zMax;

    void rcvDataCB( const sensor_msgs::LaserScanConstPtr& msg );

    void setup();

    void pack( char* params );
    void publish();
    void requestData( const re2uta_inetComms::ReqPointCloudData& msg );

    void trimPointSpacing(float leafSize);
    void convertSmallest();
    void convertSmall();
    void convertMedium();
    void convertLarge();
    void convertLargest();

//    int myPow (int x, int p);
    void toBaseOneTwentySeven(float baseTenNum, char* baseOneTwentySevenNumber);
    float toBaseTen(char* baseOneTwentySevenNumber);
    bool compareFloats (float a, float b);

    void trimPointLocation( const PointCloud::ConstPtr & inputCloud,
                       PointCloud::Ptr            & outputCloud,
                       std::string fieldName, double minValue, double maxValue, bool setNegative );
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

} // End namespace

#endif /* POINTCLOUDHANDLER_H_ */
