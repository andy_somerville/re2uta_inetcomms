/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef DATAUSAGEHANDLER_H_
#define DATAUSAGEHANDLER_H_

#include <re2uta/DataHandler.h>
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>

namespace re2uta
{

class DataUsageHandler : public DataHandler
{
public:
    DataUsageHandler( ros::NodeHandle node, ReducedMsgController *rmc );
    ~DataUsageHandler();

    void unpack( unsigned char* data );

private:
    ros::Publisher m_uiUplinkPub;
    ros::Publisher m_uiDownlinkPub;
    ros::Subscriber m_uiSub;
    ros::Subscriber m_uplinkSub;
    ros::Subscriber m_downlinkSub;

    std::string m_rosTopicPubVal2;
    std::string m_rosTopicSubDataVal2;

    std_msgs::String m_uplink;
    std_msgs::String m_downlink;

    void rcvUplinkDataCB( const std_msgs::String& msg );
    void rcvDownlinkDataCB( const std_msgs::String& msg );

    void setup();

    void pack( char* params );
    void publish();
    void requestData(const std_msgs::Empty& msg);

    void convertSmallest();
    void convertSmall();
    void convertMedium();
    void convertLarge();
    void convertLargest();

};

} // End namespace

#endif /* DATAUSAGEHANDLER_H_ */
