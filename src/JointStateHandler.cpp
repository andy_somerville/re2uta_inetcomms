/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta/JointStateHandler.h>
#include <re2uta/ReducedMsgController.h>

using namespace re2uta;

/**
 * Constructor takes in a ros node and a pointer to the ReducedMsgController
 */
JointStateHandler::JointStateHandler( ros::NodeHandle node, ReducedMsgController *rmc )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

JointStateHandler::~JointStateHandler()
{

}

/**
 * Receive the data from a ros topic (a callback function) and store it.
 * Note, just because we receive data does not mean we always want to send
 * that data over the line. Store the data until a request is made for that
 * data to be sent over the line or, if a timer is setup, when the timer
 * expires signifying the data should be sent.
 */
void JointStateHandler::rcvJSDataCB( const sensor_msgs::JointState& msg )
{
    m_rosJointState.position = msg.position;
}

/**
 * Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
 * publishes it on the appropriate ROS topic.
 */
void JointStateHandler::unpack( unsigned char* data )
{
    for ( int i = 0 ; i < 28 ; i++ )
    {
        m_packedJointState[i] = * ((unsigned short *)& (data[2 * i ]));

        m_rosJointState.position[i] = (double) (m_packedJointState[i]) * 1e-4 - M_PI;
    }

    // Publish the data
    publish();
}

/**
 * Initialize the handler.
 */
void JointStateHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    int tempHandlerId;
    m_node.param<int>("/re2uta/JointStateHandler_id", tempHandlerId, 0);

    // the topic that is published from the data handler to the OCU
    m_node.param<std::string>("/re2uta/JointStateHandler_rosTopicUIData", m_rosTopicPubVal, "/ui/jointStateHandlerData");
    // the topic that is published from the OCU to the data handler
    m_node.param<std::string>("/re2uta/JointStateHandler_rosTopicUIReq", m_rosTopicSubUIVal, "/ui/jointStateHandlerReq");
    // the topic that is published from the simulator to the data handler
    m_node.param<std::string>("/re2uta/JointStateHandler_rosTopicAtlasJointStateData", m_rosTopicSubDataVal,
                              "/atlas/joint_states");

    m_handlerId = tempHandlerId;

    // Setup the publisher and subscriber
    m_uiPub = m_node.advertise< sensor_msgs::JointState  >( m_rosTopicPubVal, 1);
    m_uiSub = m_node.subscribe( m_rosTopicSubUIVal, 1, &JointStateHandler::requestData, this );
    m_jsSub = m_node.subscribe( m_rosTopicSubDataVal, 1, &JointStateHandler::rcvJSDataCB, this );

    // pre load the joint names
    m_rosJointState.name.push_back( "back_bkz" );
    m_rosJointState.name.push_back( "back_bky" );
    m_rosJointState.name.push_back( "back_bkx" );
    m_rosJointState.name.push_back( "neck_ry" );

    m_rosJointState.name.push_back( "l_leg_hpz" );
    m_rosJointState.name.push_back( "l_leg_hpx" );
    m_rosJointState.name.push_back( "l_leg_hpy" );
    m_rosJointState.name.push_back( "l_leg_kny" );
    m_rosJointState.name.push_back( "l_leg_aky" );
    m_rosJointState.name.push_back( "l_leg_akx" );

    m_rosJointState.name.push_back( "r_leg_hpz" );
    m_rosJointState.name.push_back( "r_leg_hpx" );
    m_rosJointState.name.push_back( "r_leg_hpy" );
    m_rosJointState.name.push_back( "r_leg_kny" );
    m_rosJointState.name.push_back( "r_leg_aky" );
    m_rosJointState.name.push_back( "r_leg_akx" );

    m_rosJointState.name.push_back( "l_arm_usy" );
    m_rosJointState.name.push_back( "l_arm_shx" );
    m_rosJointState.name.push_back( "l_arm_ely" );
    m_rosJointState.name.push_back( "l_arm_elx" );
    m_rosJointState.name.push_back( "l_arm_wry" );
    m_rosJointState.name.push_back( "l_arm_wrx" );

    m_rosJointState.name.push_back( "r_arm_usy" );
    m_rosJointState.name.push_back( "r_arm_shx" );
    m_rosJointState.name.push_back( "r_arm_ely" );
    m_rosJointState.name.push_back( "r_arm_elx" );
    m_rosJointState.name.push_back( "r_arm_wry" );
    m_rosJointState.name.push_back( "r_arm_wrx" );

    m_rosJointState.position.resize(m_rosJointState.name.size());
}

/**
 * Compress, dilute, or otherwise reduce the data, then hand it off to the
 * ReducedMsgController to be sent across the line/internet.
 */
void JointStateHandler::pack( char* params )
{
    char buf[56];

    // convert the joint states into shorts
    for ( int i = 0 ; i < 28 ; i++ )
    {
        m_packedJointState[i] = (unsigned short) (10000 * (m_rosJointState.position[i] + M_PI));
        * ((unsigned short *)& (buf[2*i])) = m_packedJointState[i];
    }

    m_rmc->send((m_handlerId+1), buf, 56);
}

/**
 * Publish the most recent data on the appropriate ROS topic. This data
 * might be published once or published repeatedly via a timer.
 */
void JointStateHandler::publish()
{
    // Publish the data
    for (int i=0; i<28; i++) {
        ROS_INFO("JOINT STATE %d = %g", i, m_rosJointState.position[i]*180/M_PI);
    }
    m_uiPub.publish(m_rosJointState);
}


/**
 * Send a request to the ReducedMsgController requesting data. This should
 * be a single char (the id) as an odd number. More info can be specified
 * if you wish to only get a subset of data.
 */
void JointStateHandler::requestData(const std_msgs::Empty& msg )
{
    m_rmc->send(m_handlerId, NULL, 0);
}

void JointStateHandler::convertSmallest()
{
}
void JointStateHandler::convertSmall()
{
}
void JointStateHandler::convertMedium()
{
}
void JointStateHandler::convertLarge()
{
}
void JointStateHandler::convertLargest()
{
}
