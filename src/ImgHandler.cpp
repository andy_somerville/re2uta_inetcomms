/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta/ImgHandler.h>
#include <re2uta/ReducedMsgController.h>

//#include <cv.h>

//#include <opencv2/highgui/highgui.hpp>

#include <string.h>

using namespace re2uta;

/**
 * Constructor takes in a ros node and a pointer to the ReducedMsgController
 */
ImgHandler::ImgHandler( ros::NodeHandle node, ReducedMsgController *rmc ) :
                m_imgTransport( node )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

ImgHandler::~ImgHandler()
{
}

/**
 * Initialize the handler.
 */
void ImgHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    int tempHandlerId;
    m_node.param<int>(         "/re2uta/ImgHandler_id",                     tempHandlerId,             0                                            );
    m_node.param<std::string>( "/re2uta/ImgHandler_rosTopicUIData",         m_rosTopicPubVal,          "/ui/imgHandlerData"                         );
    m_node.param<std::string>( "/re2uta/ImgHandler_rosTopicUIReq",          m_rosTopicSubUIVal,        "/ui/imgHandlerReq"                          );
    m_node.param<std::string>( "/re2uta/ImgHandler_rosTopicAtlasHeadData",  m_rosTopicSubDataVal,      "/multisense_sl/camera/left/image_raw"       );
    m_node.param<std::string>( "/re2uta/ImgHandler_rosTopicAtlasLHandData", m_rosTopicSubLHandDataVal, "/sandia_hands/l_hand/camera/left/image_raw" );
    m_node.param<std::string>( "/re2uta/ImgHandler_rosTopicAtlasRHandData", m_rosTopicSubRHandDataVal, "/sandia_hands/r_hand/camera/left/image_raw" );
//    /sandia_hands/r_hand/camera/right/image_raw
//    /sandia_hands/r_hand/camera/left/image_raw
//    /sandia_hands/l_hand/camera/right/image_raw
//    /sandia_hands/l_hand/camera/left/image_raw
//    /multisense_sl/camera/left/image_raw
//    /multisense_sl/camera/right/image_raw

    m_handlerId = tempHandlerId;

//    sensor_msgs::ImageConstPtr msg;
//    msg->
//    m_imgHeadPtr = cv_bridge::toCvCopy( msg, sensor_msgs::image_encodings::BGR8 );
//    m_imgLHandPtr = cv_bridge::toCvCopy( msg, sensor_msgs::image_encodings::BGR8 );
//    m_imgRHandPtr = cv_bridge::toCvCopy( msg, sensor_msgs::image_encodings::BGR8 );
//    m_imgPtr = m_imgHeadPtr;

//    cv::namedWindow("Compressed (Pre-send)");

    m_imgPtr      = boost::make_shared<cv_bridge::CvImage>();
    m_imgHeadPtr  = boost::make_shared<cv_bridge::CvImage>();
    m_imgLHandPtr = boost::make_shared<cv_bridge::CvImage>();
    m_imgRHandPtr = boost::make_shared<cv_bridge::CvImage>();

//    cv_bridge::CvImage img();
//    m_imgHeadPtr.reset(img);
//    m_imgLHandPtr.reset(img);
//    m_imgRHandPtr.reset(img);
//    m_imgPtr.reset(img);

    // Setup the publisher and subscriber
    m_imgPub = m_imgTransport.advertise( m_rosTopicPubVal, 1 );
    m_uiSub = m_node.subscribe( m_rosTopicSubUIVal, 1, &ImgHandler::requestData, this );
    // Subscribe to img data from robot's camera
    m_imgHeadSub  = m_imgTransport.subscribe( m_rosTopicSubDataVal,      1, &ImgHandler::rcvHeadDataCB, this );
    m_imgLHandSub = m_imgTransport.subscribe( m_rosTopicSubLHandDataVal, 1, &ImgHandler::rcvLHandDataCB, this );
    m_imgRHandSub = m_imgTransport.subscribe( m_rosTopicSubRHandDataVal, 1, &ImgHandler::rcvRHandDataCB, this );
    // TODO: Subscribe to all cameras from robot

    setCommLvl(COMM_LVL_SMALLEST);
    setCamera(CAM_HEAD);

//    cv::namedWindow("Compressed (Pre-send)");
}

/**
 * Receive the data from a ros topic (a callback function) and store it.
 * Note, just because we receive data does not mean we always want to send
 * that data over the line. Store the data until a request is made for that
 * data to be sent over the line or, if a timer is setup, when the timer
 * expires signifying the data should be sent.
 */
void ImgHandler::rcvHeadDataCB( const sensor_msgs::ImageConstPtr& msg )
{
    ROS_INFO_ONCE( "[ImgHandler] Receiving camera data from Atlas Head" );

    // TODO: This can be more efficient by just saving the msg
    //       and converting right before sending over the line.
    try
    {
        m_imgHeadPtr = cv_bridge::toCvCopy( msg, sensor_msgs::image_encodings::BGR8 );
    }
    catch ( cv_bridge::Exception& e )
    {
        ROS_ERROR( "[ImgHandler] cv_bridge exception: %s", e.what() );
        return;
    }
}

/**
 * Receive the data from a ros topic (a callback function) and store it.
 * Note, just because we receive data does not mean we always want to send
 * that data over the line. Store the data until a request is made for that
 * data to be sent over the line or, if a timer is setup, when the timer
 * expires signifying the data should be sent.
 */
void ImgHandler::rcvLHandDataCB( const sensor_msgs::ImageConstPtr& msg )
{
    ROS_INFO_ONCE( "[ImgHandler] Receiving camera data from Atlas Left Hand" );

    // TODO: This can be more efficient by just saving the msg
    //       and converting right before sending over the line.
    try
    {
        m_imgLHandPtr = cv_bridge::toCvCopy( msg, sensor_msgs::image_encodings::BGR8 );
    }
    catch ( cv_bridge::Exception& e )
    {
        ROS_ERROR( "[ImgHandler] cv_bridge exception: %s", e.what() );
        return;
    }
}

/**
 * Receive the data from a ros topic (a callback function) and store it.
 * Note, just because we receive data does not mean we always want to send
 * that data over the line. Store the data until a request is made for that
 * data to be sent over the line or, if a timer is setup, when the timer
 * expires signifying the data should be sent.
 */
void ImgHandler::rcvRHandDataCB( const sensor_msgs::ImageConstPtr& msg )
{
    ROS_INFO_ONCE( "[ImgHandler] Receiving camera data from Atlas Right Hand" );

    // TODO: This can be more efficient by just saving the msg
    //       and converting right before sending over the line.
    try
    {
        m_imgRHandPtr = cv_bridge::toCvCopy( msg, sensor_msgs::image_encodings::BGR8 );
    }
    catch ( cv_bridge::Exception& e )
    {
        ROS_ERROR( "[ImgHandler] cv_bridge exception: %s", e.what() );
        return;
    }
}

/**
 * Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
 * publishes it on the appropriate ROS topic.
 */
void ImgHandler::unpack( unsigned char* data )
{
    ROS_INFO("[ImgHandler] Received data. Unpacking...");

    std::stringstream strData;
    std::stringstream rowSS;
    std::stringstream colSS;
    std::stringstream typSS;

    int rows;
    int cols;
    int type;
    int delimLocBegin;
    int delimLocEnd;

//    ROS_INFO( "Extracting img info" );
    strData << (data);

    // Extract the number of rows
    delimLocBegin = 0;
    delimLocEnd = strData.str().find( "|" );
    if ( delimLocBegin != delimLocEnd )
    {
        rowSS << strData.str().substr( delimLocBegin, delimLocEnd );
        rowSS >> rows;
    }

    // Extract the number of columns
    delimLocBegin = delimLocEnd + 1;
    delimLocEnd = strData.str().find( "|", delimLocBegin + 1 );
    if ( delimLocBegin != delimLocEnd )
    {
        colSS << strData.str().substr( delimLocBegin, (delimLocEnd - delimLocBegin) );
        colSS >> cols;
    }

    // Extract the img type
    delimLocBegin = delimLocEnd + 1;
    delimLocEnd = strData.str().find( "|", delimLocBegin + 1 );
    if ( delimLocBegin != delimLocEnd )
    {
        typSS << strData.str().substr( delimLocBegin, (delimLocEnd - delimLocBegin) );
        typSS >> type;
    }

    ROS_INFO("[ImgHandler] Creating img (%dx%d - %d)",cols,rows,type);
//    if (strData.str().length() > delimLocEnd+1) {
    int numLayers;
    if(type <= COMM_LVL_MEDIUM)
    {
        numLayers = 1;
    }
    else
    {
        numLayers = 3;
    }
    char imgData[cols*rows*numLayers];
    delimLocEnd = delimLocEnd + 1; // Move past the last delimiter
    for(int i=0; i<(cols*rows*numLayers); ++i)
    {
        // TODO: Err handling needed here
        imgData[i] = data[delimLocEnd];
        ++delimLocEnd;
    }



//        cv::Mat img( rows, cols, type, ((char*)strData.str().substr(delimLocEnd+1).c_str()) );
    cv::Mat img( rows, cols, type, imgData );


        
//    cvCvtColor(img,img,CV_GRAY2BGR); // cimg -> gimg
        m_imgPtr->image = img;
        
        // Set the encoding
        if(m_commLvl <= COMM_LVL_MEDIUM)
        {
            m_imgPtr->encoding = sensor_msgs::image_encodings::MONO8;
        }
        else
        {
            m_imgPtr->encoding = sensor_msgs::image_encodings::BGR8;
        }
        
        // Publish the data
    publish();

//    } else {
//        ROS_ERROR("SOMETHING WENT WRONG");
//    }
}

/**
 * Compress, dilute, or otherwise reduce the data, then hand it off to the
 * ReducedMsgController to be sent across the line/internet.
 */
void ImgHandler::pack( char* params )
{
    std::stringstream tempBuf;
    int rows;
    int cols;
    int numLayers;

    if(params[0])
    {
        int param = (int)params[0];
        int commLvl = param % 10;
        int cam = (param - commLvl)/10;

        setCommLvl(commLvl);
        setCamera(cam);
    }
    else
    {
        setCommLvl(m_commLvl); // This is probably not needed
        setCamera(m_camera); // This is needed to refresh the img
        ROS_INFO("[ImgHandler] No params given, using previous params");
    }

    // Compress img based on m_commLvl
    ROS_INFO( "[ImgHandler] Converting image using comm level %d", m_commLvl );
    switch ( m_commLvl )
    {
        case COMM_LVL_SMALLEST:
            convertSmallest();
            break;
        case COMM_LVL_SMALL:
            convertSmall();
            break;
        case COMM_LVL_MEDIUM:
            convertMedium();
            break;
        case COMM_LVL_LARGE:
            convertLarge();
            break;
        case COMM_LVL_LARGEST:
            convertLargest();
            break;
        default:
            ROS_WARN( "[ImgHandler] Unknown comm level in pack(): %d", m_commLvl );
            convertSmallest();
            break;
    }

    ROS_INFO( "[ImgHandler] Starting to pack img" );

    rows = m_imgPtr->image.rows;
    cols = m_imgPtr->image.cols;

    if(m_imgPtr->encoding == sensor_msgs::image_encodings::MONO8)
    {
        numLayers = 1;
    }
    else
    {
        // sensor_msgs::image_encodings::BGR8
        numLayers = 3;
    }

    // TODO: There is a better way to do this with less bits
    tempBuf << rows << "|";
    ROS_INFO( "[ImgHandler] %d rows packed (%d chars)", rows, (tempBuf.str().size()) );

    tempBuf << cols << "|";
    ROS_INFO( "[ImgHandler] %d columns packed (%d chars)", cols, (tempBuf.str().size()) );

    tempBuf << m_imgPtr->image.type() << "|";
    ROS_INFO( "[ImgHandler] Type packed as %d", m_imgPtr->image.type() );

    int currentIndex = tempBuf.str().size();
    char* dataOut = new char[currentIndex+(rows*cols*numLayers)+1];
    strcpy(dataOut, tempBuf.str().c_str());

//    tempBuf << m_imgPtr->image.data;
    for(int i=0; i<(rows*cols*numLayers); ++i)
    {
//        ROS_INFO("  adding %d (%d)",m_imgPtr->image.data[i], i);
//        tempBuf << m_imgPtr->image.data[i];
        dataOut[currentIndex] = m_imgPtr->image.data[i];
        ++currentIndex;
    }
//    ROS_INFO( "[ImgHandler] %d data packed (All total size: %d). Sending img!", std::strlen((char*)(m_imgPtr->image.data)) , (tempBuf.str().size()) );
    ROS_INFO( "[ImgHandler] %d data packed (Total msg size: %d). Sending img!", (rows*cols*numLayers) , currentIndex );



//    cv::imshow("Compressed (Pre-send)", m_imgPtr->image);
//    cv::waitKey(300); // time in ms
//    m_rmc->send( (m_handlerId + 1), dataOut, tempBuf.str().size() );
    m_rmc->send( (m_handlerId + 1), dataOut, currentIndex );
}

/**
 * Small resolution
 * Black and white
 * Lines (edge detection)
 */
void ImgHandler::convertSmallest()
{
    if ( m_imgPtr != 0 )
    {
        cv_bridge::CvImagePtr img;

        // Original img: 800x800 - 16
//        ROS_INFO("Img size: %dx%d", m_imgPtr->image.cols, m_imgPtr->image.rows);
//        cv::resize(m_imgPtr->image, m_imgPtr->image, cv::Size(100,100));
        cv::resize(m_imgPtr->image, m_imgPtr->image, cv::Size((m_imgPtr->image.cols/8),(m_imgPtr->image.rows/8)));

//        if(m_camera == CAM_HEAD)
        {
//            std::string cv_encoding = sensor_msgs::image_encodings::BGR8;
//            img = cv_bridge::cvtColor( m_imgPtr, cv_encoding );
//            m_imgPtr = img;

            std::string cv_encoding = sensor_msgs::image_encodings::MONO8;
            img = cv_bridge::cvtColor( m_imgPtr, cv_encoding );
            m_imgPtr = img;
        }
    }
    // NOTE: As of 4/18 uses 80,360 bits
}

/**
 * Small resolution
 * Grayscale
 */
void ImgHandler::convertSmall()
{
    if ( m_imgPtr != 0 )
    {
        cv_bridge::CvImagePtr img;

        // Original img: 800x800 - 16
//        cv::resize(m_imgPtr->image, m_imgPtr->image, cv::Size(200,200));
        cv::resize(m_imgPtr->image, m_imgPtr->image, cv::Size((m_imgPtr->image.cols/4),(m_imgPtr->image.rows/4)));

        std::string cv_encoding = sensor_msgs::image_encodings::MONO8;
        img = cv_bridge::cvtColor( m_imgPtr, cv_encoding );
        m_imgPtr = img;
    }
    // NOTE: As of 4/18 uses 321,080 bits
}

/**
 * Medium resolution
 * Grayscale
 */
void ImgHandler::convertMedium()
{
    if ( m_imgPtr != 0 )
    {
        cv_bridge::CvImagePtr img;

        // Original img: 800x800 - 16
//        cv::resize(m_imgPtr->image, m_imgPtr->image, cv::Size(400,400));
        cv::resize(m_imgPtr->image, m_imgPtr->image, cv::Size((m_imgPtr->image.cols/2),(m_imgPtr->image.rows/2)));

        std::string cv_encoding = sensor_msgs::image_encodings::MONO8;
        img = cv_bridge::cvtColor( m_imgPtr, cv_encoding );
        m_imgPtr = img;
    }
    // NOTE: As of 4/18 uses 722,256 bits
}

/**
 * Normal resolution
 * Grayscale
 */
void ImgHandler::convertLarge()
{
    // TODO Color img?
    // 448x238 often messes up

    if ( m_imgPtr != 0 )
    {
        cv_bridge::CvImagePtr img;

        // Original img: 800x800 - 16
//        cv::resize(m_imgPtr->image, m_imgPtr->image, cv::Size(100,100));
        cv::resize(m_imgPtr->image, m_imgPtr->image, cv::Size((m_imgPtr->image.cols/8),(m_imgPtr->image.rows/8)));

        std::string cv_encoding = sensor_msgs::image_encodings::BGR8;
        img = cv_bridge::cvtColor( m_imgPtr, cv_encoding );
        m_imgPtr = img;
    }
    // NOTE: As of 4/18 uses 447,008 bits
}

/**
 * Raw img from sensor
 */
void ImgHandler::convertLargest()
{
    // TODO: Does not work. Use color img?
    if ( m_imgPtr != 0 )
    {
        cv_bridge::CvImagePtr img;

        // Original img: 800x800 - 16
//        cv::resize(m_imgPtr->image, m_imgPtr->image, cv::Size(200,200));
        cv::resize(m_imgPtr->image, m_imgPtr->image, cv::Size((m_imgPtr->image.cols/4),(m_imgPtr->image.rows/4)));

        std::string cv_encoding = sensor_msgs::image_encodings::BGR8;
        img = cv_bridge::cvtColor( m_imgPtr, cv_encoding );
        m_imgPtr = img;
    }
    // NOTE: As of 4/18 uses 962,960 bits
}

/**
 * Publish the most recent data on the appropriate ROS topic. This data
 * might be published once or published repeatedly via a timer.
 */
void ImgHandler::publish()
{
    if (m_imgPtr == NULL) {
        ROS_INFO("NULL IMAGE!");
        return;
    }

    sensor_msgs::ImagePtr imsg = m_imgPtr->toImageMsg();
    m_imgPub.publish( imsg);
}

/**
 *
 */
void ImgHandler::setCamera(int newCamVal)
{
    m_camera = newCamVal;

    switch(m_camera)
    {
        case CAM_HEAD:
            m_imgPtr = m_imgHeadPtr;
            break;
        case CAM_L_HAND:
            m_imgPtr = m_imgLHandPtr;
            break;
        case CAM_R_HAND:
            m_imgPtr = m_imgRHandPtr;
            break;
        default:
            m_imgPtr = m_imgHeadPtr;
            break;
    }
}

/**
 * Send a request to the ReducedMsgController requesting data. This should
 * be a single char (the id) as an odd number. More info can be specified
 * if you wish to only get a subset of data.
 */
void ImgHandler::requestData( const re2uta_inetComms::ReqImgData& msg )
{
    ROS_INFO( "[ImgHandler] Received request for an img from UI." );

    // TODO: Fix so we can add additional params (such as the level of detail we want)
    // Level of detail (commLvl)
    // Camera (head left/right, left hand left/right, right hand left/right, etc)
    char buf[2];
    int bufIntVal;

    if( (msg.commLvl == m_commLvl) && (msg.camera == m_camera) )
    {
        m_rmc->send( m_handlerId, NULL, 0 );
    }
    else
    {
        setCommLvl(msg.commLvl);
        setCamera(msg.camera);

        bufIntVal =  m_commLvl;
        bufIntVal += 10*(m_camera);

        buf[0] = (char)bufIntVal;
        buf[1] = '\0';

        ROS_INFO("[ImgHandler] Sending req with param: %d, %d",((int)buf[0]),((int)buf[1]));

        // Send buf
        m_rmc->send( m_handlerId, buf, 2 );
        // NOTE: As of 4/18 uses 40 bits
    }
}
