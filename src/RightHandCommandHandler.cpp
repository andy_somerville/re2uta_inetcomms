/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta/RightHandCmdHandler.h>
#include <re2uta/ReducedMsgController.h>

using namespace re2uta;

/**
 * Constructor takes in a ros node and a pointer to the ReducedMsgController
 */
RightHandCmdHandler::RightHandCmdHandler( ros::NodeHandle node, ReducedMsgController *rmc )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

RightHandCmdHandler::~RightHandCmdHandler()
{
}

/**
 * Initialize the handler.
 */
void RightHandCmdHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    int tempHandlerId;
    m_node.param<int>( "/re2uta/RightHandCmdHandler_id", tempHandlerId, 0);

    // THese are the right hand commands published from the data handler to the simulator
    m_node.param<std::string>( "/re2uta/RightHandCmdHandler_rosTopicCmdToSim", m_rosTopicPubVal, 
                               "/sandia_hands/r_hand/joint_commands");

    // This is the right hand command sent from the OCU to the data handler 
    m_node.param<std::string>( "/re2uta/RightHandCmdHandler_rosTopicCmdFromOCU",  m_rosTopicSubUIVal, 
                               "/ui/right_hand_cmds");

    m_handlerId = tempHandlerId;

    // Setup the publisher
    m_cmdPub = m_node.advertise<osrf_msgs::JointCommands>( m_rosTopicPubVal, 1, this );
    // Subscribe to joint command data coming from the OCU
    m_cmdSub  = m_node.subscribe( m_rosTopicSubUIVal,  1, &RightHandCmdHandler::rcvRightHandCmdDataCB, this );

    m_rightHandCmdsToPublish.name.push_back("f0_j0");
    m_rightHandCmdsToPublish.name.push_back("f0_j1");
    m_rightHandCmdsToPublish.name.push_back("f0_j2");
    m_rightHandCmdsToPublish.name.push_back("f1_j0");
    m_rightHandCmdsToPublish.name.push_back("f1_j1");
    m_rightHandCmdsToPublish.name.push_back("f1_j2");
    m_rightHandCmdsToPublish.name.push_back("f2_j0");
    m_rightHandCmdsToPublish.name.push_back("f2_j1");
    m_rightHandCmdsToPublish.name.push_back("f2_j2");
    m_rightHandCmdsToPublish.name.push_back("f3_j0");
    m_rightHandCmdsToPublish.name.push_back("f3_j1");
    m_rightHandCmdsToPublish.name.push_back("f3_j2");

    m_rightHandCmdsToPublish.position.resize(12);
}

/**
 * Receive the right hand command data, pack and send
 */
void RightHandCmdHandler::rcvRightHandCmdDataCB( const re2uta_inetComms::JointCommand& msg )
{
    ROS_INFO_ONCE( "[RightHandCmdHandler] Received a RightHandCmd joint command" );

    m_rightHandCmdsToSend = msg;

    // pack and send it on its way
    pack(NULL);
}

/**
 * Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
 * publishes it on the appropriate ROS topic.
 */
void RightHandCmdHandler::unpack( unsigned char* data )
{
    unsigned int numCmds;
    int msgIndex = 1;
    int jointId;
    unsigned short temp;
    double dtemp;

    ROS_INFO( "Extracting RightHandCmd data" );

    numCmds = (unsigned int) data[0];

    ROS_INFO("NUM CMDS = %d", numCmds);

    for (unsigned int i=0; i<numCmds; i++) {
        jointId = data[msgIndex];

      ROS_INFO("JOINT ID = %d", data[msgIndex]);

        msgIndex++;

        ROS_INFO("UNPACK: %d %d %d", msgIndex, data[msgIndex], data[msgIndex+1]);

        temp = *((unsigned short *)& (data[msgIndex]));
        dtemp = (double) temp;
        ROS_INFO("UNPACK: %d %d %g", jointId, temp, dtemp);
        m_rightHandCmdsToPublish.position[jointId] = dtemp * 1e-4 - M_PI;
        ROS_INFO("UNPACK: %d = %g", jointId, m_rightHandCmdsToPublish.position[jointId] * 57.295791);
        msgIndex += 2;
    }

    // Publish the data
    publish();
}

/**
 * Compress, dilute, or otherwise reduce the data, then hand it off to the
 * ReducedMsgController to be sent across the line/internet.
 */
void RightHandCmdHandler::pack( char* params )
{
    char bufOut[37];   // 37 is the maximum number of bytes - could be much less
    int numCmds = 0;
    int msgIndex = 1;
    unsigned short temp;

    ROS_INFO( "[RightHandCmdHandler] Packing left hand cmd data" );

    // store any valid commands in the command message
    for (int i=0; i<12; i++) {
        if (m_rightHandCmdsToSend.valid[i] == 1) {
      
            numCmds++;

            // store the joint id
            bufOut[msgIndex] = (char) i;
            msgIndex++;

            // store the joint position
            temp = (unsigned short) (10000 * (m_rightHandCmdsToSend.cmd.position[i] + M_PI)); 
            ROS_INFO("PACK: %g = %d = %d", m_rightHandCmdsToSend.cmd.position[i], i, temp);
            *((unsigned short *)& (bufOut[msgIndex])) = temp;
            msgIndex += 2;
        }
    }
    
    // save the number of commands
    bufOut[0] = (char) numCmds;
        
    ROS_INFO("SENDING TO MESSAGE QUEUE (numCmds = %d)", numCmds);
//    m_rmc->send((m_handlerId + 1), bufOut, (3*numCmds+1));
    m_rmc->queueMsg((m_handlerId + 1), bufOut, (3*numCmds+1), "Right Hand Command");
}

/**
 * Publish the most recent data on the appropriate ROS topic. This data
 * might be published once or published repeatedly via a timer.
 */
void RightHandCmdHandler::publish()
{
    // Publish the data
    m_cmdPub.publish(m_rightHandCmdsToPublish);
}

void RightHandCmdHandler::convertSmallest()
{
}

void RightHandCmdHandler::convertSmall()
{
}

void RightHandCmdHandler::convertMedium()
{
}

void RightHandCmdHandler::convertLarge()
{
}

void RightHandCmdHandler::convertLargest()
{
}

