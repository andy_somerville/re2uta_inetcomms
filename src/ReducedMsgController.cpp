/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <sstream>

#include <re2uta/ReducedMsgController.h>
#include <re2uta/JointCmdHandler.h>
#include <re2uta/JointStateHandler.h>
#include <re2uta/ImgHandler.h>
#include <re2uta/PointCloudHandler.h>
#include <re2uta/RightHandCmdHandler.h>
#include <re2uta/LeftHandCmdHandler.h>
#include <re2uta/RightHandStateHandler.h>
#include <re2uta/LeftHandStateHandler.h>
#include <re2uta/WalkingHandler.h>
#include <re2uta/FrameCmdHandler.h>
#include <re2uta/LaserRotHandler.h>
#include <re2uta/StopCmdHandler.h>
#include <re2uta/ScoreHandler.h>
#include <re2uta/DataUsageHandler.h>
#include <re2uta/OdomHandler.h>
#include <re2uta/ObjectTrackingHandler.h>

#include <std_msgs/Int64.h>

using namespace re2uta;

/**
 * Constructor. Prepare the class to make the UDP connection.
 */
ReducedMsgController::ReducedMsgController( ros::NodeHandle node )
{
    m_node = node;
    setup();
}

/**
 * Destructor.
 */
ReducedMsgController::~ReducedMsgController()
{
    close( m_socket );
}

/**
 * Set the bit scenario to one of the 5 bit scenarios.
 * The bitScenario variable will help us keep track of how much data
 * we have used up over the course of a mission.
 *
 * @param newBitScenario the bit scenario for the given run
 */
void ReducedMsgController::setBitScenario( int newBitScenario )
{
    m_bitScenario = newBitScenario;
}

/**
 * Returns a pointer to a specific DataHandler based on the given id.
 * This will allow the UI to do fancy things like request a point cloud at
 * a specified level of detail.
 */
DataHandler* ReducedMsgController::getDataHandler( char id )
{
    DataHandler *returnValue = 0;

    // Check for even/odd and adjust the id if needed
    int evenOdd = id;
    if ( (evenOdd % 2) == 0 ) // it is even
    {
        id = evenOdd - 1; // We want odd
    }

    // TODO: this might be faster with a map... although we don't have many DH

    for ( std::vector<boost::shared_ptr<DataHandler> >::iterator currentDH = m_dataHandlers.begin() ;
            (returnValue == 0) && (currentDH != m_dataHandlers.end()) ; ++currentDH )
    {
        if ( currentDH->get()->getHandlerId() == id )
        {
            returnValue = currentDH->get();
        }
    }

    return returnValue;
}

void ReducedMsgController::queueMsg( char msgId, char* bufOut, unsigned int length, std::string description )
{
    queuedMsg newMsg;

    newMsg.msgId = msgId;
    newMsg.bufOut = new char[length];
    for (int i=0; i<length; i++) {
        newMsg.bufOut[i] = bufOut[i];
    }
    newMsg.length = length;
    newMsg.description = description;

    m_queuedMsgs.push(newMsg);
    publishMsgQueue();
}

/**
 * Send the contents of the msg queue across the internet
 */
bool ReducedMsgController::sendQueuedMsgs()
{
    bool allMsgsSent = true;
    queuedMsg currentMsg;

    while(!m_queuedMsgs.empty())
    {
        currentMsg = m_queuedMsgs.front();
        allMsgsSent = allMsgsSent & (send(currentMsg.msgId, currentMsg.bufOut, currentMsg.length));
        m_queuedMsgs.pop();
    }

    publishMsgQueue();
    return allMsgsSent;
}

void ReducedMsgController::removeQueuedMsg(int queueIndex)
{
    int msgIndex = 0;
    std::queue<queuedMsg> newQueue;

    while(!m_queuedMsgs.empty())
    {
        if(msgIndex != queueIndex)
        {
            ROS_INFO("[ReducedMsgController] %d != %d",msgIndex, queueIndex);
            newQueue.push(m_queuedMsgs.front());
        }
        else
        {
            ROS_INFO("[ReducedMsgController] %d == %d",msgIndex, queueIndex);
        }

        m_queuedMsgs.pop();
        ++msgIndex;
    }

    m_queuedMsgs = newQueue;
    publishMsgQueue();
}

void ReducedMsgController::clearQueuedMsgs()
{
    while(!m_queuedMsgs.empty())
    {
        m_queuedMsgs.pop();
    }

    publishMsgQueue();
}

/**
 * Publish the msgs queue description so that the ui can have this info
 */
void ReducedMsgController::publishMsgQueue()
{
    re2uta_inetComms::QueueInfo msg;
    std::vector<std::string> msgContents;
    std::queue<queuedMsg> tempQueue;

    while(!m_queuedMsgs.empty())
    {
        tempQueue.push(m_queuedMsgs.front());
        msgContents.push_back(m_queuedMsgs.front().description);

        m_queuedMsgs.pop();
    }
    m_queuedMsgs = tempQueue;

    msg.msgType = THIS_IS_MSG_QUEUE;
    msg.queueContents = msgContents;

    m_msgQueuePub.publish(msg);
}

/**
 * Sends a msg across the internet
 */
bool ReducedMsgController::send( char msgId, char* bufOut, unsigned int length )
{
    unsigned int i, j;
    int cnt;
    int count;
    unsigned int numMsgs;
    unsigned int sentLength;
    unsigned int lengthToSend;
    char msgBuf[MAX_BUF];

    //    ROS_INFO("In send: msgId = %d length = %d", msgId, length);

    // if too long, we'll need to change the code
    if ( length > MAX_LARGE_MSG )
    {
        ROS_ERROR( "Message too large (%d/%d), cannot send", length, MAX_LARGE_MSG );
        return false;
    }

    // stuff the msg id
    msgBuf[0] = msgId;

    // if the message needs to be split up...
    if ( length > (MAX_BUF - 3) )
    {
        sentLength = 0;
        numMsgs = (unsigned int) (length / (MAX_BUF - 3) + 1);

        // send the messages
        for ( i = 0; i < numMsgs ; i++ )
        {
            // this is the packet number
            msgBuf[1] = i + 1;

            // this is the total number of packets
            msgBuf[2] = numMsgs;

            cnt = 3;

            // determine how much data to send
            if ( length - sentLength > MAX_BUF - 3 )
            {
                lengthToSend = MAX_BUF - 3;
            }
            else
            {
                lengthToSend = length - sentLength;
            }

            // pack the message
            for ( j = sentLength; j < sentLength + lengthToSend ; j++ )
            {
                msgBuf[cnt] = bufOut[j];
                cnt++;
            }

            sentLength += lengthToSend;

            if (i == 0) {
//            for (j=0; j<20; j++) {
//                ROS_INFO("Send msgBuf %d", msgBuf[j]);
//            }
//            ROS_INFO("Cnt = %d", cnt);
            }

//            ROS_INFO("SEND: SENDING MSG %d of %d", i, numMsgs);
            usleep(500); // NOTE: THIS IS NEEDED OR IT WILL CRASH

            // if we are the client (OCU), send to the server address (robot)
            if ( true == m_client )
            {
                count = sendto( m_socket, msgBuf, cnt, 0, (struct sockaddr*)&m_serverAddr, m_addrlen );

                if ( count <= 0 )
                {
                    ROS_ERROR( "[ReducedMsgController] Message could not be sent to robot" );
                    return false;
                }
            }
            else
            {
                // else send to the client (OCU)
                count = sendto( m_socket, msgBuf, cnt, 0, (struct sockaddr*)&m_clientAddr, m_addrlen );

                if ( count <= 0 )
                {
                    ROS_ERROR( "[ReducedMsgController] Message could not be sent to OCU" );
                    return false;
                }
            }
//            ROS_INFO("  bits: %d/%d  sentLength: %d",count,m_numBitsSent,sentLength);
            m_numBitsSent += 8 * count;
//            usleep(500000);
        }
    }
    else
    {
//        ROS_INFO("[ReducedMsgController] Sending single packet");
        // there is only 1 packet
        msgBuf[1] = 1;
        msgBuf[2] = 1;

        // stuff the msg
        if ( length > 0 )
        {
            for ( i = 3; i < 3 + length ; i++ )
            {
//                ROS_INFO("bufOut[%d] = %d", i-3, bufOut[i-3]);
                msgBuf[i] = bufOut[i - 3];
            }
        }

/*
        for (unsigned int i=0; i<length + 3; i++) {
            ROS_INFO("buf[%d] = %d", i, msgBuf[i]);
        }
*/
        // if we are the client (OCU), send to the server address (robot)
        if ( true == m_client )
        {
            count = sendto( m_socket, msgBuf, length + 3, 0, (struct sockaddr*)&m_serverAddr, m_addrlen );

            if ( count <= 0 )
            {
                ROS_ERROR( "[ReducedMsgController] Message could not be sent to robot" );
                return false;
            }
        }
        else
        {
            // else send to the client (OCU)
            count = sendto( m_socket, msgBuf, length + 3, 0, (struct sockaddr*)&m_clientAddr, m_addrlen );

            if ( count <= 0 )
            {
                ROS_ERROR( "[ReducedMsgController] Message could not be sent to OCU" );
                return false;
            }
        }
        m_numBitsSent += 8 * count;
    }

    m_numMsgsSent++;


    std_msgs::Int64 msg;
    msg.data = m_numBitsSent;
    //    ROS_INFO("PUBLISHING %d BITS UPLINK", msg.data);
    m_dataUpPub.publish(msg);

    return true;

}

void ReducedMsgController::handleMsgQueueAction(const re2uta_inetComms::QueueInfoConstPtr& msg)
{
    int queueIndex;
    std::string queueIndexStr;
    std::stringstream converter;

    switch(msg->msgType)
    {
    case REQ_MSG_QUEUE:
        ROS_INFO("[ReducedMsgController] Request received for msg queue");
        publishMsgQueue();
        break;
    case EXECUTE_MSG_QUEUE:
        ROS_INFO("[ReducedMsgController] Execute queue msg received");
        sendQueuedMsgs();
        break;
    case REMOVE_FROM_MSG_QUEUE:
        ROS_INFO("[ReducedMsgController] Removing item from msg queue");
        queueIndexStr = msg->queueContents.front();
        converter << queueIndexStr;

        if ( !(converter >> queueIndex) )
        {
            queueIndex = -1;
        }
        removeQueuedMsg(queueIndex);
        break;
    case CLEAR_MSG_QUEUE:
        ROS_INFO("[ReducedMsgController] Clearing msg queue");
        clearQueuedMsgs();
        break;
    case THIS_IS_MSG_QUEUE:
        ROS_INFO("[ReducedMsgController] You should never see this msg (id=%d",msg->msgType);
        // We should never see this, but just in case, do nothing.
        break;
    default:
        ROS_WARN("[ReducedMsgController] Unknown msg type when handling msg queue msg: %d", msg->msgType);
        break;
    }
}

/**
 * Sets up the class to prepare for creating a connection. This includes
 * the setup of the handlers and establishing the bit scenario.
 */
void ReducedMsgController::setup()
{
    std::string ipAddressP;
    int port;

    // Create a publisher for publishing data usage stats
    m_dataUpPub   = m_node.advertise< std_msgs::Int64 >("/ui/dataUp",1);
    m_dataDownPub = m_node.advertise< std_msgs::Int64 >("/ui/dataDown",1);
    m_msgQueuePub = m_node.advertise< re2uta_inetComms::QueueInfo>("/ui/msgQueue",1);
    m_msgQueueSub = m_node.subscribe("/ui/msgQueueAction", 1, &ReducedMsgController::handleMsgQueueAction, this);

    // Grab the params from the param server (set via the launch file)
    m_node.param<bool>( "/re2uta/RMC_client", m_client, false );
    m_node.param<std::string>( "/re2uta/RMC_ipAddressP", ipAddressP, "127.0.0.1" );
    m_node.param<int>( "/re2uta/RMC_port", port, 1234 );
    m_node.param<int>( "/re2uta/RMC_timeout", m_timeout, 100 );

    ROS_INFO( "[RMC] Network configurations set to:" );
    if ( m_client )
    {
        ROS_INFO( "[RMC]   Client:  True" );
    }
    else
    {
        ROS_INFO( "[RMC]   Client:  False" );
    }
    ROS_INFO( "[RMC]   Dest ip: %s", ipAddressP.c_str() );
    ROS_INFO( "[RMC]   Port:    %d", port );
    ROS_INFO( "[RMC]   timeout: %d", m_timeout );

    // Create the DataHandlers
    ROS_INFO( "[RMC] Creating data handlers..." );
    ROS_INFO( "[RMC]   JointCmdHandler..." );
    boost::shared_ptr<JointCmdHandler> jointCmdHandler( new JointCmdHandler( m_node, this ) );
    ROS_INFO( "[RMC]   JointStatesHandler..." );
    boost::shared_ptr<JointStateHandler> jointStateHandler( new JointStateHandler( m_node, this ) );
    ROS_INFO( "[RMC]   ImgHandler..." );
    boost::shared_ptr<ImgHandler> imgHandler( new ImgHandler( m_node, this ) );
    ROS_INFO( "[RMC]   PointCloudHandler..." );
    boost::shared_ptr<PointCloudHandler> ptCldHandler( new PointCloudHandler( m_node, this ) );
    ROS_INFO( "[RMC]   RightHandCmdHandler..." );
    boost::shared_ptr<RightHandCmdHandler> rightHandCmdHandler( new RightHandCmdHandler( m_node, this ) );
    ROS_INFO( "[RMC]   LeftHandCmdHandler..." );
    boost::shared_ptr<LeftHandCmdHandler> leftHandCmdHandler( new LeftHandCmdHandler( m_node, this ) );
    ROS_INFO( "[RMC]   WalkingHandler..." );
    boost::shared_ptr<WalkingHandler> walkingHandler( new WalkingHandler( m_node, this ) );
    ROS_INFO( "[RMC]   RightHandStateHandler..." );
    boost::shared_ptr<RightHandStateHandler> rightHandStateHandler( new RightHandStateHandler( m_node, this ) );
    ROS_INFO( "[RMC]   LeftHandStateHandler..." );
    boost::shared_ptr<LeftHandStateHandler> leftHandStateHandler( new LeftHandStateHandler( m_node, this ) );
    ROS_INFO( "[RMC]   FrameCmdHandler..." );
    boost::shared_ptr<FrameCmdHandler> frameCmdHandler( new FrameCmdHandler( m_node, this ) );
    ROS_INFO( "[RMC]   LaserRotHandler..." );
    boost::shared_ptr<LaserRotHandler> laserRotHandler( new LaserRotHandler( m_node, this ) );
    ROS_INFO( "[RMC]   StopCmdHandler..." );
    boost::shared_ptr<StopCmdHandler> stopCmdHandler( new StopCmdHandler( m_node, this ) );
    ROS_INFO( "[RMC]   ScoreHandler..." );
    boost::shared_ptr<ScoreHandler> scoreHandler( new ScoreHandler( m_node, this ) );
    ROS_INFO( "[RMC]   DataUsageHandler..." );
    boost::shared_ptr<DataUsageHandler> dataUsageHandler( new DataUsageHandler( m_node, this ) );
    ROS_INFO( "[RMC]   OdomHandler..." );
    boost::shared_ptr<OdomHandler> odomHandler( new OdomHandler( m_node, this ) );
    ROS_INFO( "[RMC]   TrackingHandler..." );
    boost::shared_ptr<ObjectTrackingHandler> trackingHandler( new ObjectTrackingHandler( m_node, this ) );

    m_dataHandlers.clear();
    m_dataHandlers.push_back( jointCmdHandler );
    m_dataHandlers.push_back( jointStateHandler );
    m_dataHandlers.push_back( imgHandler );
    m_dataHandlers.push_back( ptCldHandler );
    m_dataHandlers.push_back( rightHandCmdHandler );
    m_dataHandlers.push_back( leftHandCmdHandler );
    m_dataHandlers.push_back( walkingHandler );
    m_dataHandlers.push_back( rightHandStateHandler );
    m_dataHandlers.push_back( leftHandStateHandler );
    m_dataHandlers.push_back( frameCmdHandler );
    m_dataHandlers.push_back( laserRotHandler );
    m_dataHandlers.push_back( stopCmdHandler );
    m_dataHandlers.push_back( scoreHandler );
    m_dataHandlers.push_back( dataUsageHandler );
    m_dataHandlers.push_back( odomHandler );
    m_dataHandlers.push_back( trackingHandler );

    ROS_INFO( "[RMC] Done creating data handlers." );

    // Initialize communications
    m_numMsgsRecvd = 0;
    m_numBitsRecvd = 0;
    m_numMsgsSent = 0;
    m_numBitsSent = 0;

    createConnection( m_client, ipAddressP, port );
}

/**
 * Creates the UDP connection between the OCU computer and the Field computer.
 *
 * @return true if all went well
 */
bool ReducedMsgController::createConnection( int client, std::string ipAddressP, int port )
{
    int status;

    m_socket = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );
//    m_socket = socket( PF_INET, SOCK_STREAM, IPPROTO_TCP );

    if ( -1 == m_socket )
    {
        ROS_ERROR( "[ReducedMsgController] Could not create socket" );
        return false;
    }

    m_myAddr.sin_family = AF_INET;
    m_myAddr.sin_port = htons( port );
    m_myAddr.sin_addr.s_addr = INADDR_ANY;

    status = bind( m_socket, (struct sockaddr*)&m_myAddr, sizeof (m_myAddr) );
    if ( -1 == status )
    {
        ROS_ERROR( "[ReducedMsgController] Could not bind socket" );
        return false;
    }

    m_addrlen = sizeof (m_myAddr);

    // some extra steps for the client side - store the server's address
    if ( true == m_client )
    {
        m_serverAddr.sin_family = AF_INET;
        inet_aton( ipAddressP.c_str(), &m_serverAddr.sin_addr );
        m_serverAddr.sin_port = htons( port );
    }

//    sudo sysctl -w net.core.rmem_max=1048576
    int n = 1048576;
    setsockopt(m_socket, SOL_SOCKET, SO_RCVBUF, &n, sizeof(n));
    setsockopt(m_socket, SOL_SOCKET, SO_SNDBUF, &n, sizeof(n));

    return true;
}

/**
 * Receives the data from the established connection and passes the
 * information on to the proper Data Handler.
 *
 * @return true if all went well
 */
bool ReducedMsgController::rcv()
{
    // Receive data from the other RMC
    //  TBD: Receving large messages
    int count;
    char packBuffer[MAX_BUF];
    char bufIn[MAX_BUF];
    unsigned char msgBuf[MAX_BUF];
    int error;

    for (int i=0; i<MAX_BUF; i++) {
        bufIn[i] = 0;
        msgBuf[i] = 0;
    }

    if ( isUDPDataThere( &error ) )
    {

        if ( true == m_client )
        {
            // if we are the client, receive from the server
            count = recvfrom( m_socket, bufIn, MAX_BUF, 0, (struct sockaddr*)&m_serverAddr, &m_addrlen );
        }
        else
        {
            // otherwise we are the server, so stored the client's address
            count = recvfrom( m_socket, bufIn, MAX_BUF, 0, (struct sockaddr*)&m_clientAddr, &m_addrlen );
        }

//       ROS_INFO("RCV: GOT %d BYTES", count);

        // no data
        if ( count <= 0 )
        {
            return false;
        }

        m_numMsgsRecvd++;
        m_numBitsRecvd += 8 * count;

        // Unpack the Handler ID from the msg
        char theID = bufIn[0];

        // If id is odd (requesting data)
        if ( theID % 2 )
        {
            std::copy(bufIn+3,bufIn+MAX_BUF-3, packBuffer);
            getDataHandler( theID )->pack(packBuffer);
        }
        else
        {
            // if it is just one packet, stuff and call unpack
            // NOTE: THIS DOES NOT CONTAIN THE MSG ID IN THE FIRST BYTE!
            if ( bufIn[2] == 1 )
            {
                for ( int i = 0 ; i < count - 3 ; i++ )
                {
                    msgBuf[i] = (unsigned char) bufIn[i + 3];
            //                    ROS_INFO("RCV: msgBuf[%d] = %d", i, msgBuf[i]);
                }

                std_msgs::Int64 msg;
                msg.data = m_numBitsRecvd;
        //        ROS_INFO("PUBLISHING %d BITS DOWNLINK", msg.data);
                m_dataDownPub.publish(msg);

                getDataHandler( theID )->unpack( msgBuf );
            }
            else
            {
                m_largeMsgID = theID;

                // save the message info
                unsigned char packet = (unsigned char) bufIn[1];
                unsigned char numPackets = (unsigned char) bufIn[2];
//                ROS_INFO("[ReducedMessageController] Recieved packet %d, %d",packet, numPackets);
                if (packet < 0 || numPackets < 0) {
                    ROS_INFO("WARNING: Packet %d or numPackets %d is negative!", packet, numPackets);
                    return false; 
                }

                // if it's the first packet, reset
                if (packet == 1) {
                    for (int i=0; i<MAX_LARGE_MSG; i++) {
                        m_largeMsgBuffer[i] = 0;
                    }
                }
                    
                // copy the data
                for ( int i = 0 ; i < count - 3 ; i++ )
                {
                    m_largeMsgBuffer[(packet-1)*(MAX_BUF-3)+i] = (unsigned char) bufIn[i + 3];
                }

                // if it's the last packet
               if (packet == numPackets )
               {
                    std_msgs::Int64 msg;
//                    ROS_INFO("NUM BITS RECEIVED = %d", m_numBitsRecvd);
                    msg.data = m_numBitsRecvd;
            //            ROS_INFO("PUBLISHING %d BITS DOWNLINK", msg.data);
                    m_dataDownPub.publish(msg);

                    getDataHandler( theID )->unpack( m_largeMsgBuffer );
                }
            }
        }
    }

    return true;
}

/**
 * Returns the number of messages and bits sent and received
 */
void ReducedMsgController::getMessageStats( int *numMsgsRecvd, int *numBitsRecvd, int *numMsgsSent, int *numBitsSent )
{
    *numMsgsRecvd = m_numMsgsRecvd;
    *numBitsRecvd = m_numBitsRecvd;
    *numMsgsSent = m_numMsgsSent;
    *numBitsSent = m_numBitsSent;
}

///**
// * ROS message handlers
// */
//#if 0
// TODO: Remove this. This logic should live in the JointStateDataHandler
//void ReducedMsgController::handleReqJointStateMsg(const ?_msgs::ReqJointState::ConstPtr &rjs)
//{
//  char buf[REQ_JOINT_STATE_MSG_LENGTH];
//
//  DataHandler *jsDataHandler = getDataHandler("JointState");
//
//  // this gets the message data for the request message
//  jsDataHandler->requestData(buf);
//
//  send(buf);
//}
//
//void ReducedMsgController::handleJointStateMsg(const sensor_msgs::JointState::ConstPtr &js)
//{
//  DataHandler *jsDataHandler = getDataHandler("JointState");
//
//  // this gets the message data for the request message
//  jsDataHandler->rcvData(&js);
//}
//#endif

/**
 * Checks if UDP data is available to read. Waits for timeout millisecs
 */
int ReducedMsgController::isUDPDataThere( int *error )
{
    fd_set socketReadSet;
    struct timeval tv;

    FD_ZERO( &socketReadSet );
    FD_SET( m_socket, &socketReadSet );

    tv.tv_sec = m_timeout / 1000;
    tv.tv_usec = (m_timeout % 1000) * 1000;

    if ( select( m_socket + 1, &socketReadSet, 0, 0, &tv ) == -1 )
    {
        *error = 1;
        return 0;
    }

    *error = 0;

    return (FD_ISSET( m_socket, &socketReadSet ) != 0);
}

/*
 * sets number of bits sent (uplink) using the number published by the actual VRC code
 */
void ReducedMsgController::setNumBitsSent(int n)
{
  //  ROS_INFO("Setting numBitsSent to %d", n);
  m_numBitsSent = n;
  std_msgs::Int64 msg;
  msg.data = m_numBitsSent;
  ROS_INFO("PUBLISHING %d BITS UPLINK", msg.data);
  m_dataUpPub.publish(msg);

}

/*
 * sets number of bits received (downlink) using the number published by the actual VRC code
 */
void ReducedMsgController::setNumBitsRcvd(int n)
{
  //  ROS_INFO("Setting numBitsRecvd to %d", n);
  m_numBitsRecvd = n;
  std_msgs::Int64 msg;
  msg.data = m_numBitsRecvd;
  ROS_INFO("PUBLISHING %d BITS DOWNLINK", msg.data);
  m_dataDownPub.publish(msg);

}

/**
 * Main function. Boots up the ROS stuff needed to make this code
 * run as a node.
 */
int main( int argc, char *argv[] )
{
    ros::init( argc, argv, "ReducedMsgController" );
    ros::NodeHandle node;

    ROS_INFO( "Started Reduced Message Controller" );
    ReducedMsgController rmc( node );

    while ( ros::ok() )
    {
        ros::spinOnce();

        // check for UDP data
        rmc.rcv();
    }

    ROS_INFO( "Shutting down Reduced Message Controller..." );
}
