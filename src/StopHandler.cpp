/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta/StopCmdHandler.h>
#include <re2uta/ReducedMsgController.h>

using namespace re2uta;

/**
 * Constructor takes in a ros node and a pointer to the ReducedMsgController
 */
StopCmdHandler::StopCmdHandler( ros::NodeHandle node, ReducedMsgController *rmc )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

StopCmdHandler::~StopCmdHandler()
{
}

/**
 * Initialize the handler.
 */
void StopCmdHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    int tempHandlerId;
    m_node.param<int>( "/re2uta/StopCmdHandler_id", tempHandlerId, 0);

    // This is the stop command published from the data handler to the simulator
    m_node.param<std::string>( "/re2uta/StopCmdHandler_rosTopicCmdToSim", m_rosTopicPubVal, 
                               "/commander/stop_cmd");

    // This is the stop command sent from the OCU to the data handler 
    m_node.param<std::string>( "/re2uta/StopCmdHandler_rosTopicCmdFromOCU",  m_rosTopicSubUIVal, 
                               "/ui/stop_cmd");

    m_handlerId = tempHandlerId;

    // Setup the publisher - this publishes to the sim
    m_cmdPub = m_node.advertise<std_msgs::Empty>( m_rosTopicPubVal, 1 );

    // Subscribe to joint command data coming from the OCU
    m_cmdSub  = m_node.subscribe( m_rosTopicSubUIVal,  1, &StopCmdHandler::rcvStopCmdDataCB, this );
}

/**
 * Receive the stop command data from the OCU, pack and send
 */
void StopCmdHandler::rcvStopCmdDataCB( const std_msgs::Empty& msg )
{
  //    ROS_INFO( "[StopHandler] Received a StopCmd from OCU" );

    // pack and send it on its way
    pack(NULL);
}

/**
 * Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
 * publishes it on the appropriate ROS topic.
 */
void StopCmdHandler::unpack( unsigned char* data )
{
    // Publish the data
    publish();
}

/**
 * Compress, dilute, or otherwise reduce the data, then hand it off to the
 * ReducedMsgController to be sent across the line/internet.
 */
void StopCmdHandler::pack( char* params )
{
    m_rmc->send((m_handlerId + 1), NULL, 0);
}

/**
 * Publish the most recent data on the appropriate ROS topic. This data
 * might be published once or published repeatedly via a timer.
 */
void StopCmdHandler::publish()
{
    std_msgs::Empty stopMsg;
    // Publish the data
    //    ROS_INFO("[StopHandler]: Publishing stop message");
    m_cmdPub.publish(stopMsg);
}

void StopCmdHandler::convertSmallest()
{
}

void StopCmdHandler::convertSmall()
{
}

void StopCmdHandler::convertMedium()
{
}

void StopCmdHandler::convertLarge()
{
}

void StopCmdHandler::convertLargest()
{
}

