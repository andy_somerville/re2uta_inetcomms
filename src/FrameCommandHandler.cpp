/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta/FrameCmdHandler.h>
#include <re2uta/ReducedMsgController.h>

using namespace re2uta;

/**
 * Constructor takes in a ros node and a pointer to the ReducedMsgController
 */
FrameCmdHandler::FrameCmdHandler( ros::NodeHandle node, ReducedMsgController *rmc )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

FrameCmdHandler::~FrameCmdHandler()
{
}

/**
 * Initialize the handler.
 */
void FrameCmdHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    int tempHandlerId;
    m_node.param<int>( "/re2uta/FrameCmdHandler_id", tempHandlerId, 0);

    // These are the joint commands published from the data handler to the simulator
    m_node.param<std::string>( "/re2uta/FrameCmdHandler_rosTopicCmdToSim", m_rosTopicPubVal, 
                               "/commander/command_frame_pose");

    // This is the joint command sent from the OCU to the data handler 
    m_node.param<std::string>( "/re2uta/FrameCmdHandler_rosTopicCmdFromOCU",  m_rosTopicSubUIVal, "/ui/frame_cmd");

    m_handlerId = tempHandlerId;

    // Setup the publisher - this publishes to the sim
    m_cmdPub = m_node.advertise<re2uta_atlasCommander::FrameCommandStamped>( m_rosTopicPubVal, 1 );
    // Subscribe to joint command data coming from the OCU
    m_cmdSub  = m_node.subscribe( m_rosTopicSubUIVal,  1, &FrameCmdHandler::rcvFrameCmdDataCB, this );
}

/**
 * Receive the frame command data from the OCU, pack and send
 */
void FrameCmdHandler::rcvFrameCmdDataCB( const re2uta_atlasCommander::FrameCommandStamped& msg )
{
//    ROS_INFO( "[FrameCmdHandler] Received a FrameCmd from OCU" );

    m_frameCmdToPublish = msg;

    // pack and send it on its way
    pack(NULL);
}

/**
 * Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
 * publishes it on the appropriate ROS topic.
 */
void FrameCmdHandler::unpack( unsigned char* data )
{
    for (int i=0; i<16; i++) {
        ROS_INFO("RCV[%d]: %d", i, data[i]);
    }

    m_frameCmdToPublish.header.frame_id = lookupFrameName(data[0]);
    m_frameCmdToPublish.target_frame_id = lookupFrameName(data[1]);
    m_frameCmdToPublish.goalPose.header.frame_id = lookupFrameName(data[2]);

    // weights
    m_frameCmdToPublish.weights.linear.x = (data[3] & 0x0F) / 30.0;;
    m_frameCmdToPublish.weights.linear.y = (data[3] & 0xF0) / 480.0;
    m_frameCmdToPublish.weights.linear.z = (data[4] & 0x0F) / 30.0;
    m_frameCmdToPublish.weights.angular.x = (data[4] & 0xF0) / 480.0;
    m_frameCmdToPublish.weights.angular.y = (data[5] & 0x0F) / 30.0;
    m_frameCmdToPublish.weights.angular.z = (data[5] & 0xF0) / 480.0;
    
    // translations +/16 meters at millimeter resolution
    unsigned short temp;

    temp = *((unsigned short *)& (data[6]));
    m_frameCmdToPublish.goalPose.pose.position.x = (double) (temp) / 300 - 100.0;

    temp = *((unsigned short *)& (data[8]));
    m_frameCmdToPublish.goalPose.pose.position.y = (double) (temp) / 300 - 100.0;

    temp = *((unsigned short *)& (data[10]));
    m_frameCmdToPublish.goalPose.pose.position.z = (double) (temp) / 300 - 100.0;

    double rotx, roty, rotz;
    temp = *((unsigned short *)& (data[12]));
    m_frameCmdToPublish.goalPose.pose.orientation.x = (double) (temp) * 1e-4 - 1.0;

    temp = *((unsigned short *)& (data[14]));
    m_frameCmdToPublish.goalPose.pose.orientation.y = (double) (temp) * 1e-4 - 1.0;

    temp = *((unsigned short *)& (data[16]));
    m_frameCmdToPublish.goalPose.pose.orientation.z = (double) (temp) * 1e-4 - 1.0;

    temp = *((unsigned short *)& (data[18]));
    m_frameCmdToPublish.goalPose.pose.orientation.w = (double) (temp) * 1e-4 - 1.0;

    
    ROS_INFO("RECVD MSG");
    ROS_INFO("BASE FRAME: %s", m_frameCmdToPublish.header.frame_id.c_str());
    ROS_INFO("TIP FRAME: %s", m_frameCmdToPublish.target_frame_id.c_str());
    ROS_INFO("GOAL FRAME: %s", m_frameCmdToPublish.goalPose.header.frame_id.c_str());
    ROS_INFO("WEIGHTS: %g %g %g %g %g %g",
             m_frameCmdToPublish.weights.linear.x,
             m_frameCmdToPublish.weights.linear.y,
             m_frameCmdToPublish.weights.linear.z,
             m_frameCmdToPublish.weights.angular.x,
             m_frameCmdToPublish.weights.angular.y,
             m_frameCmdToPublish.weights.angular.z);
    ROS_INFO("POSE: (%g %g %g)  (%g %g %g %g)",
             m_frameCmdToPublish.goalPose.pose.position.x,
             m_frameCmdToPublish.goalPose.pose.position.y,
             m_frameCmdToPublish.goalPose.pose.position.z,
             m_frameCmdToPublish.goalPose.pose.orientation.x,
             m_frameCmdToPublish.goalPose.pose.orientation.y,
             m_frameCmdToPublish.goalPose.pose.orientation.z,
             m_frameCmdToPublish.goalPose.pose.orientation.w);

    // Publish the data
    publish();
}

/**
 * Compress, dilute, or otherwise reduce the data, then hand it off to the
 * ReducedMsgController to be sent across the line/internet.
 */
void FrameCmdHandler::pack( char* params )
{
    char bufOut[20];  


#if 0
    // REMOVE THIS!!!!!!!
    m_frameCmdToPublish.header.frame_id = "l_foot";
    m_frameCmdToPublish.target_frame_id = "l_hand";
    m_frameCmdToPublish.goalPose.header.frame_id = "odom";
    m_frameCmdToPublish.weights.linear.x = 0.3;
    m_frameCmdToPublish.weights.linear.y = 0.3;
    m_frameCmdToPublish.weights.linear.z = 0.3;
    m_frameCmdToPublish.weights.angular.x = 0;
    m_frameCmdToPublish.weights.angular.y = 0;
    m_frameCmdToPublish.weights.angular.z = 0;
    m_frameCmdToPublish.goalPose.pose.position.x = 0.16;
    m_frameCmdToPublish.goalPose.pose.position.y = 0.371;
    m_frameCmdToPublish.goalPose.pose.position.z = 0.81;
    m_frameCmdToPublish.goalPose.pose.orientation.x = 0.106;
    m_frameCmdToPublish.goalPose.pose.orientation.y = 0.086;
    m_frameCmdToPublish.goalPose.pose.orientation.z = -0.706;
    m_frameCmdToPublish.goalPose.pose.orientation.w = 0.694;
#endif

    ROS_INFO("SENDING MSG");
    ROS_INFO("BASE FRAME: %s", m_frameCmdToPublish.header.frame_id.c_str());
    ROS_INFO("TIP FRAME: %s", m_frameCmdToPublish.target_frame_id.c_str());
    ROS_INFO("GOAL FRAME: %s", m_frameCmdToPublish.goalPose.header.frame_id.c_str());
    ROS_INFO("WEIGHTS: %g %g %g %g %g %g",
             m_frameCmdToPublish.weights.linear.x,
             m_frameCmdToPublish.weights.linear.y,
             m_frameCmdToPublish.weights.linear.z,
             m_frameCmdToPublish.weights.angular.x,
             m_frameCmdToPublish.weights.angular.y,
             m_frameCmdToPublish.weights.angular.z);
    ROS_INFO("POSE: %g %g %g %g %g %g %g",
             m_frameCmdToPublish.goalPose.pose.position.x,
             m_frameCmdToPublish.goalPose.pose.position.y,
             m_frameCmdToPublish.goalPose.pose.position.z,
             m_frameCmdToPublish.goalPose.pose.orientation.x,
             m_frameCmdToPublish.goalPose.pose.orientation.y,
             m_frameCmdToPublish.goalPose.pose.orientation.z,
         m_frameCmdToPublish.goalPose.pose.orientation.w);

    //ROS_INFO( "[FrameCmdHandler] Packing frame cmd data" );
    
    // frame ids 
    bufOut[0] = lookupFrameId(m_frameCmdToPublish.header.frame_id);
    bufOut[1] = lookupFrameId(m_frameCmdToPublish.target_frame_id);
    bufOut[2] = lookupFrameId(m_frameCmdToPublish.goalPose.header.frame_id);

    // weights (NOTE: ASSUMES MAX VALUE IS 0.5)
    bufOut[3] = (int) (30.0 * m_frameCmdToPublish.weights.linear.x);
    bufOut[3] += 16 * (int) (30.0 * m_frameCmdToPublish.weights.linear.y);
    bufOut[4] = (int ) (30.0 * m_frameCmdToPublish.weights.linear.z);   
    bufOut[4] += 16 * (int) (30.0 * m_frameCmdToPublish.weights.angular.x);   
    bufOut[5] = (int) (30.0  * m_frameCmdToPublish.weights.angular.y); 
    bufOut[5] += 16 * (int) (30.0 * m_frameCmdToPublish.weights.angular.z); 
    
    // translations +/16 meters at millimeter resolution
    unsigned short temp;

    temp = (unsigned short) ((m_frameCmdToPublish.goalPose.pose.position.x + 100) * 300);
    *((unsigned short *)& (bufOut[6])) = temp;

    temp = (unsigned short) ((m_frameCmdToPublish.goalPose.pose.position.y + 100) * 300);
    *((unsigned short *)& (bufOut[8])) = temp;

    temp = (unsigned short) ((m_frameCmdToPublish.goalPose.pose.position.z + 100) * 300);
    *((unsigned short *)& (bufOut[10])) = temp;

    // rotations - should be a unit quaternion from -1 to 1
    temp = (unsigned short) ((m_frameCmdToPublish.goalPose.pose.orientation.x + 1.0) * 10000);
    *((unsigned short *)& (bufOut[12])) = temp;

    temp = (unsigned short) ((m_frameCmdToPublish.goalPose.pose.orientation.y + 1.0)  * 10000);
    *((unsigned short *)& (bufOut[14])) = temp;

    temp = (unsigned short) ((m_frameCmdToPublish.goalPose.pose.orientation.z + 1.0) * 10000);
    *((unsigned short *)& (bufOut[16])) = temp;

    temp = (unsigned short) ((m_frameCmdToPublish.goalPose.pose.orientation.w + 1.0) * 10000);
    *((unsigned short *)& (bufOut[18])) = temp;

    for (int i=0; i<20; i++) {
        ROS_INFO("SEND[%d]: %d", i, bufOut[i]);
    }

//    m_rmc->send((m_handlerId + 1), bufOut, 20);
    m_rmc->queueMsg((m_handlerId + 1), bufOut, 20, "Frame Command");
}

/**
 * Publish the most recent data on the appropriate ROS topic. This data
 * might be published once or published repeatedly via a timer.
 */
void FrameCmdHandler::publish()
{
    // Publish the data
    ROS_INFO("PUBLISHING DATA");
    m_cmdPub.publish(m_frameCmdToPublish);
}

void FrameCmdHandler::convertSmallest()
{
}

void FrameCmdHandler::convertSmall()
{
}

void FrameCmdHandler::convertMedium()
{
}

void FrameCmdHandler::convertLarge()
{
}

void FrameCmdHandler::convertLargest()
{
}

std::string FrameCmdHandler::lookupFrameName(char id)
{
    std::string ret;

    if (id == 0) {
        ret = "head";
    } else if (id == 1) {
        ret = "l_clav";
    } else if (id == 2) {
        ret = "l_farm";
    } else if (id == 3) {
        ret = "l_foot";
    } else if (id == 4) {
        ret = "l_hand";
    } else if (id == 5) {
        ret = "l_larm";
    } else if (id == 6) {
        ret = "l_lglut";
    } else if (id == 7) {
        ret = "l_lleg";
    } else if (id == 8) {
        ret = "l_scap";
    } else if (id == 9) {
        ret = "l_talus";
    } else if (id == 10) {
        ret = "l_uarm";
    } else if (id == 11) {
        ret = "l_uglut";
    } else if (id == 12) {
        ret = "l_uleg";
    } else if (id == 13) {
        ret = "ltorso";
    } else if (id == 14) {
        ret = "mtorso";
    } else if (id == 15) {
        ret = "pelvis";
    } else if (id == 16) {
        ret = "r_clav";
    } else if (id == 17) {
        ret = "r_farm";
    } else if (id == 18) {
        ret = "r_foot";
    } else if (id == 19) {
        ret = "r_hand";
    } else if (id == 20) {
        ret = "r_larm";
    } else if (id == 21) {
        ret = "r_lglut";
    } else if (id == 22) {
        ret = "r_lleg";
    } else if (id == 23) {
        ret = "r_scap";
    } else if (id == 24) {
        ret = "r_talus";
    } else if (id == 25) {
        ret = "r_uarm";
    } else if (id == 26) {
        ret = "r_uglut";
    } else if (id == 27) {
        ret = "r_uleg";
    } else if (id == 28) {
        ret = "utorso";
    } else if (id == 29) {
        ret = "hose";
    } else if (id == 30) {
        ret = "pipe";
    } else if (id == 31) {
        ret = "valve";
    } else if (id == 32) {
      ret = "odom";
    }

    return ret;
}

char FrameCmdHandler::lookupFrameId(std::string name)
{
    char ret;
    
    if (name == "head") {
        ret = 0;
    } else if (name == "l_clav") {
        ret = 1;
    } else if (name == "l_farm") {
        ret = 2;
    } else if (name == "l_foot") {
        ret = 3;
    } else if (name == "l_hand") {
        ret = 4;
    } else if (name == "l_larm") {
        ret = 5;
    } else if (name == "l_lglut") {
        ret = 6;
    } else if (name == "l_lleg") {
        ret = 7;
    } else if (name == "l_scap") {
        ret = 8;
    } else if (name == "l_talus") {
        ret = 9;
    } else if (name == "l_uarm") {
        ret = 10;
    } else if (name == "l_uglut") {
        ret = 11;
    } else if (name == "l_uleg") {
        ret = 12;
    } else if (name == "ltorso") {
        ret = 13;
    } else if (name == "mtorso") {
        ret = 14;
    } else if (name == "pelvis") {
        ret = 15;
    } else if (name == "r_clav") {
        ret = 16;
    } else if (name == "r_farm") {
        ret = 17;
    } else if (name == "r_foot") {
        ret = 18;
    } else if (name == "r_hand") {
        ret = 19;
    } else if (name == "r_larm") {
        ret = 20;
    } else if (name == "r_lglut") {
        ret = 21;
    } else if (name == "r_lleg") {
        ret = 22;
    } else if (name == "r_scap") {
        ret = 23;
    } else if (name == "r_talus") {
        ret = 24;
    } else if (name == "r_uarm") {
        ret = 25;
    } else if (name == "r_uglut") {
        ret = 26;
    } else if (name == "r_uleg") {
        ret = 27;
    } else if (name == "utorso") {
        ret = 28;
    } else if (name == "hose") {
        ret = 29;
    } else if (name == "pipe") {
        ret = 30;
    } else if (name == "valve") {
        ret = 31;
    } else if (name == "odom") {
      ret = 32;
    }

    return ret;
}
